package nachos.vm;

public class IntPair {
	public int a, b;
	public IntPair(int x, int y) {
		a = x;
		b = y;
	}
	
	public boolean equals(Object o) {
		if (o == null || !(o instanceof IntPair))
			return false;
		return a == ((IntPair)o).a && b == ((IntPair)o).b;
	}
	
	@Override
	public String toString() {
		return String.valueOf(a) + " " + String.valueOf(b);
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
}
