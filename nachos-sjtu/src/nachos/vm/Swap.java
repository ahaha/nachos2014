package nachos.vm;

import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.threads.ThreadedKernel;

public class Swap {

	public Swap() {
	}

	public static void init() {
		swapFile.write(new byte[pageSize], 0, pageSize);
	}
	
	public static void close() {
		if (swapFile == null)
			return ;
		swapFile.close();
		ThreadedKernel.fileSystem.remove(swapFileName);
		swapFile = null;
	}
	
	public static byte[] read(int pid, int vpn) {
		Integer pos = lookUp(pid, vpn);
		byte[] mem = new byte[pageSize];
		if (pos == null) {
//			System.err.println("read error: " + vpn);
			return mem;
		}
		if (swapFile.read(pos * pageSize, mem, 0, pageSize) == -1)
			return new byte[pageSize];
		return mem;
	}
	
	public static int write(int pid, int vpn, byte[] mem, int offset) {
		Integer pos = lookUp(pid, vpn);
//		System.err.println(" " + pid + " " + vpn + " " + pos);
		if (pos == null) {
			pos = allocate();
//			System.err.println("new swap map: pid " + pid + " vpn " + vpn + " pos " + pos);
			swapMap.put(new IntPair(pid, vpn), pos);
		}
		swapFile.write(pos * pageSize, mem, offset, pageSize);
		return pos;
	}
	
	public static void free(int pid, int vpn) {
		Integer pos = swapMap.remove(new IntPair(pid, vpn));
		if (pos != null)
			freePage.add(pos);
	}
	
	private static Integer allocate() {
		if (freePage.isEmpty())
			return pageCount++;
		else
			return freePage.removeFirst();
	}

	private static Integer lookUp(int pid, int vpn) {
		return swapMap.get(new IntPair(new Integer(pid), new Integer(vpn)));
	}

	
	private static LinkedList<Integer> freePage = new LinkedList<Integer>();
	private static HashMap<IntPair, Integer> swapMap = new HashMap<IntPair, Integer>();

	private static int pageCount = 0;
	private static int pageSize = Processor.pageSize;
	private static String swapFileName = "SWAP";
	
	public static OpenFile swapFile = ThreadedKernel.fileSystem.open(swapFileName, true);

}
