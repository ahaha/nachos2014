package nachos.vm;

import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		for (int i = 0; i < Machine.processor().getTLBSize(); i++)
			savedTLB[i] = new TranslationEntry(0, 0, false, false, false, false);
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	@Override
	public void saveState() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
			TranslationEntry e = Machine.processor().readTLBEntry(i);
			if (e.valid)
				PageTable.update(pid, e);
			savedTLB[i] = e;
		}
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	@Override
	public void restoreState() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
			TranslationEntry t = new TranslationEntry(0, 0, false, false, false, false);
			if (savedTLB[i].valid) {
				TranslationEntry e = PageTable.get(pid, savedTLB[i].vpn);
				if (e != null && e.valid)
					t = savedTLB[i];
			}
			Machine.processor().writeTLBEntry(i, t);
		}
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	@Override
	protected boolean loadSections() {
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection coffSection = coff.getSection(s);
			int vpn = coffSection.getFirstVPN();
			for (int i = 0; i < coffSection.getLength(); i++)
				lazySection.put(new Integer(vpn + i), new IntPair(new Integer(s), new Integer(i))); // ?
		}
		return true;
	}

	@Override
	protected boolean allocatePages(int vpn, int pageCount, boolean readOnly) {
		for (int i = 0; i < pageCount; i++) {
			TranslationEntry e = new TranslationEntry(vpn + i, 0, false, readOnly, false, false);
			pageLock.acquire();
			PageTable.add(pid, e);
			pageLock.release();
			localPages.add(new Integer(vpn + i));
		}
		numPages += pageCount;
		return true;
	}
	
	@Override
	protected void releasePages() {
		for (Integer vpn : localPages) {
			pageLock.acquire();
			TranslationEntry e = PageTable.remove(pid, vpn);
			if (e != null && e.valid)
				VMKernel.deletePage(e.ppn);
			Swap.free(pid, vpn);
			pageLock.release();
		}
		return ;
	}
	
	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		super.unloadSections();
	}
	
	@Override
	protected TranslationEntry translate(int vaddr) {
		return PageTable.get(pid, Processor.pageFromAddress(vaddr));
	}
	
	@Override
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		pageLock.acquire();
		trySwap(Processor.pageFromAddress(vaddr));
		TranslationEntry e = translate(vaddr);
		e.used = true;
		PageTable.set(pid, e);
		pageLock.release();
		return super.readVirtualMemory(vaddr, data, offset, length);
	}
	
	@Override
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		pageLock.acquire();
		trySwap(Processor.pageFromAddress(vaddr));
		TranslationEntry e = translate(vaddr);
		e.used = true;
		e.dirty = true;
		PageTable.set(pid, e);
		pageLock.release();
		return super.writeVirtualMemory(vaddr, data, offset, length);
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	@Override
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			pageLock.acquire();
			if (!handleTLBMiss(processor.readRegister(Processor.regBadVAddr))) {
				pageLock.release();
				handleExit(-1);
			} else {
				pageLock.release();
			}
			break;
			
		default:
			super.handleException(cause);
			break;
		}
	}
	
	private boolean handleTLBMiss(int vaddr) {
//		printTLB();
//		printPageTable();
		TranslationEntry e = translate(vaddr);
		if (e != null) {
//			System.err.println("TLBMiss: " + vaddr + " pid: " + pid + " vpn: " + e.vpn);
			if (!e.valid) {
				trySwap(e.vpn);
				e = translate(vaddr);
			}
			int i = chooseTLB();
			updateTLB(i, e);
			return true;
		}
		return false;
	}

	private int trySwap(int vpn) {
		TranslationEntry e = PageTable.get(new Integer(pid), vpn);
		if (e.valid) {
			return e.ppn;
		}
		int ppn = newPage();
		swapIn(vpn, ppn);
		return ppn;
	}

	private int newPage() {
		int ppn = VMKernel.newPage();
		if (ppn == -1) {
			ppn = PageTable.choose();
			IntPair ip = PageTable.coreMap[ppn];
			if (ip != null) {
				int pid = ip.a, vpn = ip.b;
				swapOut(pid, vpn);
			}
		}
		return ppn;
	}

	private void swapIn(int vpn, int ppn) {
//		System.err.println("Swap in vpn: " + vpn + " ppn: " + ppn);
		TranslationEntry e = PageTable.get(pid, vpn);
		boolean updated = false;
		if (lazySection.containsKey(new Integer(vpn))) {
//			System.err.println("lazy load pid " + pid + " vpn " + vpn + " ppn " + ppn);
			lazyLoad(vpn, ppn);
			updated = true;
		} else {
			byte[] page = Swap.read(pid, vpn);
			byte[] memory = Machine.processor().getMemory();
			System.arraycopy(page, 0, memory, ppn * pageSize, pageSize);
		}
		PageTable.set(pid, new TranslationEntry(vpn, ppn, true, e.readOnly, updated, updated));
//		printPageTable();
	}

	private void lazyLoad(int vpn, int ppn) {
		IntPair ip = lazySection.remove(new Integer(vpn));
		if (ip != null) {
			int sn = ip.a, pn = ip.b;
			coff.getSection(sn).loadPage(pn, ppn);
		}
	}

	private void swapOut(int pid, int vpn) {
		TranslationEntry e = PageTable.get(pid, vpn);
//		System.err.println("Swap out pid: " + pid + " vpn: " + vpn + " ppn: " + e.ppn);
		TranslationEntry t = null;
		int tlbIndex = -1;
		for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
			t = Machine.processor().readTLBEntry(i);
			if (t.vpn == e.vpn && t.ppn == e.ppn && t.valid) { // !
				tlbIndex = i;
				break;
			}
		}
		if (tlbIndex != -1) {
			PageTable.update(pid, t);
			t.valid = false;
			Machine.processor().writeTLBEntry(tlbIndex, t);
		}
		e = PageTable.get(pid, vpn);
		
		if (e.dirty) {
			byte[] memory = Machine.processor().getMemory();
			Swap.write(pid, vpn, memory, e.ppn * pageSize);
		}
		e.valid = false;
		PageTable.set(pid, e);
	}

	private void updateTLB(int i, TranslationEntry e) {
		TranslationEntry t = Machine.processor().readTLBEntry(i);
		if (t.valid)
			PageTable.update(pid, t);
		Machine.processor().writeTLBEntry(i, e);
	}

	private int chooseTLB() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++)
			if (!Machine.processor().readTLBEntry(i).valid)
				return i;
		return Lib.random(Machine.processor().getTLBSize());
	}
	
	public static void printTLB() {
		for (int i = 0; i < Machine.processor().getTLBSize(); i++) {
			TranslationEntry e = Machine.processor().readTLBEntry(i);
			System.err.println(i + " " + e.ppn + " " + e.vpn + " " + e.valid);
		}
	}
	
	public static void printPageTable() {
		System.err.println("PageTable");
		for (IntPair ip : PageTable.pageTable.keySet()) {
			TranslationEntry e = PageTable.pageTable.get(ip);
			System.err.println(ip + " " + e.ppn + " " + e.vpn + " " + e.valid);
		}
		System.err.println("");
	}


	private static Lock pageLock = new Lock();
	private HashMap<Integer, IntPair> lazySection = new HashMap<Integer, IntPair>();
	private LinkedList<Integer> localPages = new LinkedList<Integer>();
	private TranslationEntry[] savedTLB = new TranslationEntry[Machine.processor().getTLBSize()];

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
}
