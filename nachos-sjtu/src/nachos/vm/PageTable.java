package nachos.vm;

import java.util.HashMap;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.TranslationEntry;

public class PageTable {

	public static TranslationEntry merge(TranslationEntry e1, TranslationEntry e2) {
		TranslationEntry r = new TranslationEntry(e1);
		r.used |= e2.used;
		r.dirty |= e2.dirty;
		return r;
	}
	
	public static boolean add(int pid, TranslationEntry e) {
		IntPair ip = new IntPair(pid, e.vpn);
		if (pageTable.containsKey(ip))
			return false;
		pageTable.put(ip, new TranslationEntry(e));
		if (e.valid)
			coreMap[e.ppn] = ip;
		return true;
	}
	
	public static TranslationEntry remove(int pid, int vpn) {
//		System.err.println("remove pid " + pid + " vpn " + vpn);
		TranslationEntry e = pageTable.remove(new IntPair(pid, vpn));
		if (e != null && e.valid) // ?
			coreMap[e.ppn] = null;
		return e;
	}
	
	public static TranslationEntry get(int pid, int vpn) {
		IntPair ip = new IntPair(pid, vpn);
		if (!pageTable.containsKey(ip)) {
//			VMProcess.printPageTable();
			return null;
		}
		return new TranslationEntry(pageTable.get(ip));
	}
	
	public static void update(int pid, TranslationEntry e) {
		_set(pid, e, true);
	}
	
	public static void set(int pid, TranslationEntry e) {
		_set(pid, e, false);
	}
	
	private static void _set(int pid, TranslationEntry e, boolean update) {
		IntPair ip = new IntPair(pid, e.vpn);
		if (!pageTable.containsKey(ip))
			return ;
		TranslationEntry t = pageTable.get(ip);
		TranslationEntry s = update ? new TranslationEntry(merge(e, t)) : new TranslationEntry(e);
		if (t.valid)
			coreMap[t.ppn] = null;
		if (e.valid)
			coreMap[s.ppn] = ip;
		pageTable.put(ip, s);
	}
	
	public static int choose() {
		int ppn;
		TranslationEntry e;
		do {
			ppn = Lib.random(Machine.processor().getNumPhysPages());
			IntPair ip = coreMap[ppn];
			if (ip == null)
				return ppn;
			e = pageTable.get(ip);
		} while (e == null || !e.valid);
		return ppn;
	}

	public static IntPair[] coreMap = new IntPair[Machine.processor().getNumPhysPages()];
	public static HashMap<IntPair, TranslationEntry> pageTable = new HashMap<IntPair, TranslationEntry>();
}
