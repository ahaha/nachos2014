package nachos.filesys;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;
import nachos.threads.Lock;
import nachos.vm.Swap;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
	/** the free list */
	private static FreeList free_list;
	
	/** the root folder */
	private Folder root_folder;
	
	/** the current folder */
	private Folder cur_folder;
	
	/** the string representation of the current folder */
//	LinkedList<String> cur_path = new LinkedList<String>();
	
	private static HashMap<Integer, INode> inodes = new HashMap<Integer, INode>();
	private static Hashtable<INode, Folder> folders = new Hashtable<INode, Folder>();
	
	/**
	 * initialize the file system
	 * 
	 * @param format
	 *					whether to format the file system
	 */
	public void init (boolean format)
	{
		if (format)
		{
				INode inode_free_list = new INode(FreeList.STATIC_ADDR);
				inode_free_list.file_type = INode.TYPE_SYSTEM;
				inodes.put(new Integer(FreeList.STATIC_ADDR), inode_free_list);
				free_list = new FreeList(inode_free_list);
				free_list.init();
				inode_free_list.save();
				
				INode inode_root_folder = new INode(Folder.STATIC_ADDR);
				inode_root_folder.file_type = INode.TYPE_SYSTEM;
				inodes.put(new Integer(Folder.STATIC_ADDR), inode_root_folder);
				root_folder = new Folder(inode_root_folder, null, "/");
				root_folder.save();
				folders.put(inode_root_folder, root_folder);
				inode_root_folder.save();
				cur_folder = root_folder;
//				cur_path.add("/");
				
				Swap.init();
				
				importStub();
		}
		else
		{
				INode inode_free_list = new INode(FreeList.STATIC_ADDR);
				inode_free_list.load();
				inodes.put(new Integer(FreeList.STATIC_ADDR), inode_free_list);
				free_list = new FreeList(inode_free_list);
				free_list.load();
				
				INode inode_root_folder = new INode(Folder.STATIC_ADDR);
				inode_root_folder.load();
				inodes.put(new Integer(Folder.STATIC_ADDR), inode_root_folder);
				root_folder = new Folder(inode_root_folder, null, "/");
				folders.put(inode_root_folder, root_folder);
				inode_root_folder.load();
				cur_folder = root_folder;
//				cur_path.add("/");

				Swap.init();
				
				importStub();
		}
	}
	
	public void finish ()
	{
		root_folder.save();
		free_list.save();
		for (INode inode : inodes.values())
			inode.save();
	}
	
	/** import from stub filesystem */
	private void importStub ()
	{
		FileSystem stubFS = Machine.stubFileSystem();
		FileSystem realFS = FilesysKernel.realFileSystem;
		String[] file_list = Machine.stubFileList();
		for (int i = 0; i < file_list.length; ++i)
		{
			if (!file_list[i].endsWith(".coff"))
				continue;
			OpenFile src = stubFS.open(file_list[i], false);
			if (src == null)
			{
				continue;
			}
			OpenFile dst = realFS.open(file_list[i], true);
			int size = src.length();
			byte[] buffer = new byte[size];
			src.read(0, buffer, 0, size);
			dst.write(0, buffer, 0, size);
			src.close();
			dst.close();
		}
	}
	
	/** get the only free list of the file system */
	public FreeList getFreeList ()
	{
		return free_list;
	}
	
	/** get the only root folder of the file system */
	public Folder getRootFolder ()
	{
		return root_folder;
	}
	
	public OpenFile open(String name, boolean create)
	{
		Path path = Path.parse(name);
		if (path == null)
			return null;
		if (path.inode != null) {
			// found file
//			System.err.println(path.inode.file_type == INode.TYPE_FILE);
			if (path.inode.file_type == INode.TYPE_FILE) {
				path.inode.use_count++;
				if (create)
					path.inode.link_count++;
				return new File(path.inode);
			} else if (path.inode.file_type == INode.TYPE_SYMLINK) {
//				System.err.println("READING SYMLINK");
				File f = new File(path.inode);
				byte[] l = new byte[4];
				f.read(l, 0, 4);
				int len = Lib.bytesToInt(l, 0);
				byte[] s = new byte[len];
				f.read(s, 0, len);
				return open(new String(s), create);
			}
		} else if (create) {
			if (path.parent != null) {
				INode inode = getINode(path.parent.create(path.filename));
				inode.use_count++;
				inode.link_count++;
				return new File(inode);
			}
		}
		return null;
	}
	
	public boolean remove (String name)
	{
		Path path = Path.parse(name);
		if (path != null && path.inode != null) {
//			System.err.println("REMOVE " + name + " SIZE " + path.inode.getSectorNum());
			if (path.inode.file_type == INode.TYPE_FILE || path.inode.file_type == INode.TYPE_SYMLINK) {
				path.parent.removeEntry(path.filename);
				if ((--path.inode.link_count) == 0)
					path.inode.file_type = INode.TYPE_FILE_DEL;
				path.inode._free();
				return true;
			}
		}
		return false;
	}
	
	public boolean createFolder (String name)
	{
		Path path = Path.parse(name);
		if (path == null || path.inode != null)
			return false;
		path.parent.createFolder(path.filename).link_count = 1;
		return true;
	}
	
	public boolean removeFolder (String name)
	{
		Path path = Path.parse(name);
		if (path == null || path.inode == null)
			return false;
		// check name is "/"
		// rm "./" or "../" ?
		if (path.inode.file_type == INode.TYPE_FOLDER) {
			Folder folder = getFolder(path.inode, null, "");
			folder.load();
			if (folder.isEmpty()) {
//				System.err.println("RMDIR " + path.filename);
				path.parent.removeEntry(path.filename);
				path.parent.save();
				path.inode.link_count--;
				path.inode.file_type = INode.TYPE_FOLDER_DEL;
				path.inode._free();
				return true;
			}
		}
		return false;
	}
	
	public boolean changeCurFolder (String name)
	{
		Path path = Path.parse(name);
		if (path == null || path.inode == null)
			return false;
		if (path.inode.file_type == INode.TYPE_FOLDER) {
			Folder folder = getFolder(path.inode, null, "");
			folder.load();
//			System.err.println("CHDIR TO " + name + " (" + folder + ")");
			cur_folder = folder;
//			cur_path.add(path.filename);
			return true;
		}
		return false;
	}
	
	public String[] readDir (String name)
	{
		Path path = Path.parse(name);
		if (path == null || path.inode == null)
			return null;
		if (path.inode.file_type == INode.TYPE_FOLDER) {
			Folder folder = getFolder(path.inode, null, "");
			folder.load();
			return folder.list();
		}
		return null;
	}
	
	public FileStat getStat (String name)
	{
		Path path = Path.parse(name);
		if (path == null || path.inode == null)
			return null;
		FileStat stat = new FileStat();
		stat.name = path.filename;
		stat.inode = path.inode.getAddr();
		stat.links = path.inode.link_count;
		if (path.inode.file_type == INode.TYPE_FILE) {
			stat.type = FileStat.NORMAL_FILE_TYPE;
		} else if (path.inode.file_type == INode.TYPE_FOLDER) {
			stat.type = FileStat.DIR_FILE_TYPE;
		} else if (path.inode.file_type == INode.TYPE_SYMLINK) {
			stat.type = FileStat.LinkFileType;
		} else return null;
		stat.size = path.inode.file_size;
		stat.sectors = path.inode.getSectorNum();
		return stat;
	}
	
	public boolean createLink (String src, String dst)
	{
		Path src_path = Path.parse(src);
		if (src_path == null || src_path.inode == null)
			return false;
		Path dst_path = Path.parse(dst);
		if (dst_path == null || dst_path.parent == null || dst_path.inode != null)
			return false;
		src_path.inode.link_count++;
		dst_path.parent.createLink(dst_path.filename, src_path.inode);
		return true;
	}
	
	public boolean createSymlink (String src, String dst)
	{
		Path src_path = Path.parse(src);
		if (src_path == null || src_path.inode == null)
			return false;
		Path dst_path = Path.parse(dst);
		if (dst_path == null || dst_path.parent == null || dst_path.inode != null)
			return false;
		dst_path.parent.createSymlink(dst_path.filename, src).link_count = 1;
		return true;
	}
	
	public int getFreeSize()
	{
//		System.err.println(free_list.getSize());
		return free_list.getSize();
	}
	
	public int getSwapFileSectors()
	{
		return ((File)Swap.swapFile).inode.getSectorNum();
	}
	
	public static INode removeINode(Integer addr)
	{
		return inodes.remove(addr);
	}
	
	public static INode getINode(Integer addr)
	{
		INode inode = inodes.get(addr);
		if (inode == null) {
			inode = new INode(addr);
			if (free_list.used(addr))
				inode.load();
			inodes.put(addr, inode);
		}
		return inode;
	}
	
	public static Folder removeFolder(INode inode) {
		return folders.remove(inode);
	}
	
	public static Folder getFolder(INode inode, Folder parent, String name) {
		Folder folder = folders.get(inode);
		if (folder == null) {
			Lib.assertTrue(parent != null && !name.isEmpty());
			folder = new Folder(inode, parent, name);
			folders.put(inode, folder);
		} else {
			Lib.assertTrue(parent == null && name.isEmpty());
		}
		return folder;
	}

	public String getCWD() {
		String cwd = cur_folder.toString();
		while (cwd.length() > 1 && cwd.endsWith("/"))
			cwd = cwd.substring(0, cwd.length() - 1);
//		System.err.println(cwd);
		return cwd;
	}
}
