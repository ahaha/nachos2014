package nachos.filesys;

import java.util.Hashtable;

import nachos.machine.Lib;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
	/** the static address for root folder */
	public static int STATIC_ADDR = 1;
	
	private int size;
	
	/** mapping from filename to folder entry */
	private Hashtable<String, FolderEntry> entry;
	
	public Folder parent;
	
	public Folder (INode inode, Folder parent, String name)
	{
		super(inode, name);
		size = 4;
		entry = new Hashtable<String, FolderEntry>();
		inode.file_type = INode.TYPE_FOLDER;
		if (parent == null)
			this.parent = this;
		else
			this.parent = parent;
	}
	
	/** open a file in the folder and return its address */
	public int open (String filename)
	{
		FolderEntry e = entry.get(filename);
		if (e == null)
			return -1;
		return e.addr;
	}
	
	/** create a new file in the folder and return its address */
	public int create (String filename)
	{
		int addr = FilesysKernel.realFileSystem.getFreeList().allocate();
		INode inode = RealFileSystem.getINode(new Integer(addr));
		inode.file_type = INode.TYPE_FILE;
		addEntry(filename, addr);
		return addr;
	}
	
	public INode createFolder (String filename)
	{
//		System.err.println("MKDIR " + filename + " IN " + name);
		int addr = FilesysKernel.realFileSystem.getFreeList().allocate();
		INode inode = RealFileSystem.getINode(new Integer(addr));
		inode.file_type = INode.TYPE_FOLDER;
		addEntry(filename, addr);
		Folder f = RealFileSystem.getFolder(inode, this, filename);
		f.save();
		return inode;
	}
	
	public INode createLink (String filename, INode dst)
	{
		addEntry(filename, dst.getAddr());
		return dst;
	}
	
	public INode createSymlink (String filename, String dst)
	{
		int addr = FilesysKernel.realFileSystem.getFreeList().allocate();
		INode inode = RealFileSystem.getINode(new Integer(addr));
		inode.file_type = INode.TYPE_SYMLINK;
		addEntry(filename, addr);
		File f = new File(inode);
//		System.err.println(filename + " -> " + dst);
		f.write(Lib.bytesFromInt(dst.length()), 0, 4);
		f.write(dst.getBytes(), 0, dst.length());
		return inode;
	}
		
	/** add an entry with specific filename and address to the folder */
	public void addEntry (String filename, int addr)
	{
		entry.put(filename, new FolderEntry(filename, addr));
	}
	
	public FolderEntry getEntry(String filename) {
//		System.err.println("FINDING " + filename + " IN " + entry + "(" + inode + ")");
		return entry.get(filename);
	}
	
	/** remove an entry from the folder */
	public void removeEntry (String filename)
	{
		entry.remove(filename);
	}
	
	/** save the content of the folder to the disk */
	public void save ()
	{
		//TODO implement this
	}
	
	/** load the content of the folder from the disk */
	public void load ()
	{
		//TODO implement this
	}

	public boolean isEmpty() {
		return entry.isEmpty();
	}

	public String[] list() {
		if (entry.isEmpty())
			return null;
		String[] items = new String[entry.size()];
		int i = 0;
		for (String e : entry.keySet())
			items[i++] = e;
		return items;
	}
	
	public String toString() {
		if (parent == this)
			return getName();
		return parent + getName() + "/";
	}
}
