package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.threads.Lock;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
	/** represent a system file (free list) */
	public static int TYPE_SYSTEM = 0;
	
	/** represent a folder */
	public static int TYPE_FOLDER = 1;
	
	/** represent a normal file */
	public static int TYPE_FILE = 2;
	
	/** represent a normal file that is marked as delete */
	public static int TYPE_FILE_DEL = 3;
	
	/** represent a symbolic link file */
	public static int TYPE_SYMLINK = 4;
	
	/** represent a folder that are not valid */
	public static int TYPE_FOLDER_DEL = 5;
	
	/** the reserve size (in byte) in the first sector */
	private static final int FIRST_SEC_RESERVE = 16;
	
	/** size of the file in bytes */
	int file_size;
	
	/** the type of the file */
	int file_type;
	
	/** the number of programs that have access on the file */
	int use_count;
	
	/** the number of links on the file */
	int link_count;
	
	/** maintain all the sector numbers this file used in order */
	private LinkedList<Integer> sec_addr;
	
	/** the first address */
	private int addr;
	
	/** the extended address */
	private LinkedList<Integer> addr_ext;
	
	public Lock readLock = new Lock();
	public Lock writeLock = new Lock();
	
	public INode (int addr)
	{
		file_size = 0;
		file_type = TYPE_FILE;
		use_count = 0;
		link_count = 0;
		sec_addr = new LinkedList<Integer>();
		this.addr = addr;
		addr_ext = new LinkedList<Integer>();
	}
	
	/** get the sector number of a position in the file	*/
	public int getSector (int pos)
	{
		return sec_addr.get(pos);
	}
	
	/** change the file size and adjust the content in the inode accordingly */
	public void setFileSize (int size)
	{
		int sector_num = Lib.divRoundUp(size, Disk.SectorSize);
		if (file_size != size) {
//			System.err.println("FROM " + file_size + " TO " + size + " WHEN #free_sector = " + FilesysKernel.realFileSystem.getFreeSize() + " #sec_addr = " + sec_addr.size() + " #sector_num = " + sector_num);
		}
		file_size = size;
		if (sector_num < sec_addr.size()) {
			for (int i = sector_num; i < sec_addr.size(); i++)
				FilesysKernel.realFileSystem.getFreeList().deallocate(sec_addr.removeLast());
		} else if (sector_num > sec_addr.size()) {
			for (int i = sec_addr.size(); i < sector_num; i++)
				sec_addr.add(FilesysKernel.realFileSystem.getFreeList().allocate());
		}
	}
	
	/** free the disk space occupied by the file (including inode) */
	public void free ()
	{
//		System.err.println("FREE " + this);
		readLock.acquire();
		writeLock.acquire();
		for (Integer i : addr_ext)
			FilesysKernel.realFileSystem.getFreeList().deallocate(i);
		addr_ext.clear();
		for (Integer i : sec_addr)
			FilesysKernel.realFileSystem.getFreeList().deallocate(i);
		sec_addr.clear();
		FilesysKernel.realFileSystem.getFreeList().deallocate(addr);
		RealFileSystem.removeINode(addr);
		readLock.release();
		writeLock.release();
	}
	
	/** load inode content from the disk */
	public void load ()
	{
		//TODO implement this
	}
	
	/** save inode content to the disk */
	public void save ()
	{
		//TODO implement this
	}

	public void _free() {
//		System.err.println("TRY FREE " + this);
		if (use_count == 0 && link_count == 0)
			free();
	}
	
	public int getAddr() {
		return addr;
	}
	
	public int getSectorNum() {
		return sec_addr.size();
	}
	
	public String toString() {
		return "@" + addr;
	}

}
