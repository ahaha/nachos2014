package nachos.filesys;

import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
	protected static final int SYSCALL_MKDIR = 14;
	protected static final int SYSCALL_RMDIR = 15;
	protected static final int SYSCALL_CHDIR = 16;
	protected static final int SYSCALL_GETCWD = 17;
	protected static final int SYSCALL_READDIR = 18;
	protected static final int SYSCALL_STAT = 19;
	protected static final int SYSCALL_LINK = 20;
	protected static final int SYSCALL_SYMLINK = 21;
	
	public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
	{
//		System.err.println("" + syscall + " " + a0 + " " + a1 + " " + a2 + " " + a3);
		switch (syscall)
		{
			case SYSCALL_MKDIR:
			{
				return handleMkdir(a0);
			}
				
			case SYSCALL_RMDIR:
			{
				return handleRmdir(a0);
			}
				
			case SYSCALL_CHDIR:
			{
				return handleChdir(a0);
			}
				
			case SYSCALL_GETCWD:
			{
				return handleGetcwd(a0, a1);
			}
				
			case SYSCALL_READDIR:
			{
				return handleReaddir(a0, a1, a2, a3);
			}
				
			case SYSCALL_STAT:
			{
				return handleStat(a0, a1);
			}
			 
			case SYSCALL_LINK:
			{
				return handleLink(a0, a1);
			}
			
			case SYSCALL_SYMLINK:
			{
				return handleSymlink(a0, a1);
			}
			
			default:
				return super.handleSyscall(syscall, a0, a1, a2, a3);
		}
	}
	
	private int handleSymlink(int a0, int a1) {
		String src = readVirtualMemoryString(a0, argLenMax);
		String dst = readVirtualMemoryString(a1, argLenMax);
		return FilesysKernel.realFileSystem.createSymlink(src, dst) ? 0 : -1;
	}

	private int handleLink(int a0, int a1) {
		String src = readVirtualMemoryString(a0, argLenMax);
		String dst = readVirtualMemoryString(a1, argLenMax);
		return FilesysKernel.realFileSystem.createLink(src, dst) ? 0 : -1;
	}

	private int handleStat(int a0, int a1) {
		String path = readVirtualMemoryString(a0, argLenMax);
		FileStat stat = FilesysKernel.realFileSystem.getStat(path);
		if (stat == null)
			return -1;
		return writeVirtualMemory(a0, stat.toString().getBytes());
	}

	private int handleReaddir(int dir, int buf, int count, int size) {
		String path = readVirtualMemoryString(dir, argLenMax);
		String[] list = FilesysKernel.realFileSystem.readDir(path);
		if (list == null || list.length > count)
			return -1;
		byte[] data = new byte[count * size];
		for (int i = 0; i < list.length; i++) {
			if (list[i].length() >= size)
				return -1;
			System.arraycopy((list[i] + '\0').getBytes(), 0, data, size * i, list[i].length() + 1);
		}
		int res = writeVirtualMemory(buf, data);
		if (res < 0)
			return res;
		return list.length;
	}

	private int handleGetcwd(int a0, int a1) {
		String path = FilesysKernel.realFileSystem.getCWD() + '\0';
		if (path.length() >= a1)
			return -1;
		return writeVirtualMemory(a0, path.getBytes());
	}

	private int handleChdir(int a0) {
		return FilesysKernel.realFileSystem.changeCurFolder(readVirtualMemoryString(a0, argLenMax)) ? 0 : -1;
	}

	private int handleRmdir(int a0) {
		return FilesysKernel.realFileSystem.removeFolder(readVirtualMemoryString(a0, argLenMax)) ? 0 : -1;
	}

	private int handleMkdir(int a0) {
		return FilesysKernel.realFileSystem.createFolder(readVirtualMemoryString(a0, argLenMax)) ? 0 : -1;
	}

	/*
	public void handleException (int cause)
	{
		if (cause == Processor.exceptionSyscall)
		{
			//TODO implement this
		}
		else
			super.handleException(cause);
	}
		*/
}
