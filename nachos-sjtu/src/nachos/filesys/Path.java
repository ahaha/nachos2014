package nachos.filesys;

import java.util.LinkedList;

public class Path {
	Folder parent;
	String filename;
	INode inode;
	
	public static int maxPathLen = 255;
	
	Path() {
	}
	
	public static Path parse(String path)
	{
		Path P = new Path();
		while (path.length() > 1 && path.endsWith("/"))
			path.substring(0, path.length() - 1);
		
		if (!path.startsWith("/")) {
			path = FilesysKernel.realFileSystem.getCWD() + "/" + path;
		}
		
		if (path.equals("/")) {
			P.filename = "/";
			P.parent = FilesysKernel.realFileSystem.getRootFolder();
			P.inode = P.parent.inode;
			return P;
		}
		
		String[] dirs = path.split("/");
		P.filename = dirs[dirs.length - 1];
		Folder folder = FilesysKernel.realFileSystem.getRootFolder();

		INode inode = null;
//		System.err.println(path);
		for (int i = 0; i < dirs.length; i++) {
//			System.err.println(i + " " + folder);
			String s = dirs[i];
			if (s.isEmpty() || s.equals("."))
				continue ;
			if (s.equals("..")) {
				folder = folder.parent;
				P.parent = folder;
				inode = folder.inode;
				continue ;
			}
			FolderEntry e = folder.getEntry(s);
			P.parent = folder;
			if (e == null) {
//				System.err.println("NOT FOUND " + s + s+ " IN " + folder);
//				if (P.parent.list() != null) for (String ss : P.parent.list()) System.err.println(ss);
				P.parent = folder;
				if (i != dirs.length - 1)
					return null;
				return P;
			}
			inode = RealFileSystem.getINode(new Integer(e.addr)); // assume no symlink to folder
			if (inode.file_type == INode.TYPE_FOLDER) {
				Folder f = RealFileSystem.getFolder(inode, null, "");
				folder = f;
			} else break;
		}
//		P.parent = folder;
		P.inode = inode;
		return P;
	}

}
