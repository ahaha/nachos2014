package nachos.filesys;

import java.util.LinkedList;
import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
	/** the static address */
	public static int STATIC_ADDR = 0;
	
	/** size occupied in the disk (bitmap) */
	static int size = Lib.divRoundUp(Disk.NumSectors, 8);
	
	/** maintain address of all the free sectors */
	private LinkedList<Integer> free_list;
	
	public FreeList (INode inode)
	{
	  super(inode);
	  free_list = new LinkedList<Integer>();
	}
	
	public void init ()
	{
	  for (int i = 0; i < Disk.NumSectors; ++i)
	    free_list.add(i);
	}
	
	/** allocate a new sector in the disk */
	public int allocate ()
	{
	  Lib.assertTrue(!free_list.isEmpty(), "no available sector");
	  return free_list.removeFirst();
	}
	
	/** deallocate a sector to be reused */
	public void deallocate (int sec)
	{
		free_list.add(new Integer(sec));
	}
	
	/** save the content of freelist to the disk */
	public void save ()
	{
	  //TODO implement this
	}
	
	/** load the content of freelist from the disk */
	public void load ()
	{
	  //TODO implement this
	}

	public boolean used(Integer addr) {
		return !free_list.contains(addr);
	}
	
	public int getSize() {
		/*
		for (int i = 0; i < Disk.NumSectors; i++)
			if (used(new Integer(i)))
				System.err.print(i + " ");
		System.err.println("");
			*/
		return free_list.size();
	}
}
