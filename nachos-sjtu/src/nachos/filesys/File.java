package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
	INode inode;
	
	private int pos;
	
	public File (INode inode)
	{
		this.inode = inode;
		pos = 0;
	}
	
	public File (INode inode, String name)
	{
		super(null, name);
		this.inode = inode;
		pos = 0;
	}
	
	public int length ()
	{
		return inode.file_size;
	}
	
	public void close ()
	{
		if (inode == null)
		  return ;
		inode.use_count--;
//	  System.err.println("USE " + inode.use_count + " LINK " + inode.link_count);
		inode._free();
		inode = null;
	}
	
	public void seek (int pos)
	{
		this.pos = pos;
	}
	
	public int tell ()
	{
		return pos;
	}
	
	public int read (byte[] buffer, int start, int limit)
	{
		int ret = read(pos, buffer, start, limit);
		pos += ret;
		return ret;
	}
	
	public int write (byte[] buffer, int start, int limit)
	{
//	  System.err.println("WRITE NOW " + pos + " " + start + " " + limit);

		int ret = write(pos, buffer, start, limit);
		pos += ret;
		return ret;
	}
	
	public int read (int pos, byte[] buffer, int start, int _limit)
	{
//	  System.err.println("READ " + pos + " " + start + " " + _limit);
		if (pos >= inode.file_size) {
		  return -1;
		}
		inode.readLock.acquire();
		int limit = Math.min(inode.file_size - pos, _limit);
		byte[] data = new byte[Disk.SectorSize];
		int count = 0;
		int sector_offset = pos % Disk.SectorSize, buffer_offset = start;
		int sector_begin = pos / Disk.SectorSize, sector_end = Lib.divRoundUp(pos + limit, Disk.SectorSize);
		for (int i = sector_begin; i < sector_end; i++) {
		  Machine.synchDisk().readSector(inode.getSector(i), data, 0);
		  int len = Math.min(limit, Disk.SectorSize - sector_offset);
		  System.arraycopy(data, sector_offset, buffer, buffer_offset, len);
		  sector_offset = 0;
		  buffer_offset += len;
		  limit -= len;
		  count += len;
		}
		inode.readLock.release();
		return count;
	}
	
	public int write (int pos, byte[] buffer, int start, int _limit)
	{
		inode.readLock.acquire();
		inode.writeLock.acquire();
		if (pos + _limit > inode.file_size) {
		  inode.setFileSize(pos + _limit);
		}
		int limit = Math.min(inode.file_size - pos, _limit);
		byte[] data = new byte[Disk.SectorSize];
		int count = 0;
		int sector_offset = pos % Disk.SectorSize, buffer_offset = start;
		int sector_begin = pos / Disk.SectorSize, sector_end = Lib.divRoundUp(pos + limit, Disk.SectorSize);
		for (int i = sector_begin; i < sector_end; i++) {
		  Machine.synchDisk().readSector(inode.getSector(i), data, 0);
		  int len = Math.min(limit, Disk.SectorSize - sector_offset);
//		  System.err.println("BUF " + new String(buffer) + " #" + buffer_offset + " DATA " + new String(data) + " #" + sector_offset + " LEN " + len);
		  System.arraycopy(buffer, buffer_offset, data, sector_offset, len);
		  Machine.synchDisk().writeSector(inode.getSector(i), data, 0);
		  sector_offset = 0;
		  buffer_offset += len;
		  limit -= len;
		  count += len;
		}
		inode.readLock.release();
		inode.writeLock.release();
		return count;
	}
}
