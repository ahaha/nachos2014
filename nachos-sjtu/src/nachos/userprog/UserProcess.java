package nachos.userprog;

import nachos.filesys.FilesysKernel;
import nachos.machine.*;
import nachos.threads.*;

import java.io.EOFException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {

	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		int numPhysPages = Machine.processor().getNumPhysPages();
		pageTable = new TranslationEntry[numPhysPages];
		for (int i = 0; i < numPhysPages; i++)
			pageTable[i] = new TranslationEntry(i, i, true, false, false, false);
		
		fdinfo = new HashMap<Integer, OpenFile>();
		fdinfo.put(new Integer(0), UserKernel.console.openForReading());
		fdinfo.put(new Integer(1), UserKernel.console.openForWriting());
		fdNum = 2;
		
		lockProcess.acquire();
		pid = ++pidNum;
		proc.put(new Integer(pid), this);
		lockProcess.release();
				
		parent = null;
		childs = new HashMap<Integer, UserProcess>();
		
		finished = new Semaphore(0);
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		if (!load(name, args))
			return false;

		new UThread(this).setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Machine.processor().setPageTable(pageTable);
	}
	
	protected boolean allocatePages(int vpn, int pageCount, boolean readOnly) {
		if (vpn + pageCount >= pageTable.length)
			return false;
		Integer[] allocated = new Integer[pageCount];
		for (int i = 0; i < pageCount; i++) {
			int ppn = UserKernel.newPage();
			if (ppn == -1) {
				for (int j = 0; j < i; j++)
					UserKernel.deletePage(allocated[j]);
				return false;
			}
			allocated[i] = ppn;
		}
		for (int i = 0; i < pageCount; i++)
			pageTable[vpn + i] = new TranslationEntry(vpn + i, allocated[i], true, readOnly, false, false);
		numPages += pageCount;
		return true;
	}
	
	protected void releasePages() {
		for (int i = 0; i < pageTable.length; i++)
			if (pageTable[i].valid) {
				UserKernel.deletePage(pageTable[i].ppn);
				pageTable[i] = new TranslationEntry(pageTable[i].vpn, 0, false, false, false, false);
			}
		numPages = 0;
	}

	protected TranslationEntry translate(int vaddr) {
		return lookupPageTable(Processor.pageFromAddress(vaddr));
	}

	
	private TranslationEntry lookupPageTable(int vpn) {
		if (pageTable == null)
			return null;
		if (vpn >= 0 && vpn < pageTable.length)
			return pageTable[vpn];
		return null;
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

//		System.err.println("READ VM@" + vaddr);
		TranslationEntry t = translate(vaddr);
		if (t == null || !t.valid)
			return 0;
		
		int page = t.ppn;
		int voffset = Processor.offsetFromAddress(vaddr);
		int paddr = Processor.makeAddress(page, voffset);
		
		byte[] memory = Machine.processor().getMemory();

		int amount = Math.min(length, pageSize - voffset);
		System.arraycopy(memory, paddr, data, offset, amount);
		if (amount < length)
			return amount + readVirtualMemory(Processor.makeAddress(t.vpn + 1, 0), data, offset + amount, length - amount);
		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		TranslationEntry t = translate(vaddr);
		if (t == null || !t.valid)
			return 0;
		
		int page = t.ppn;
		int voffset = Processor.offsetFromAddress(vaddr);
		int paddr = Processor.makeAddress(page, voffset);
			
		byte[] memory = Machine.processor().getMemory();

		int amount = Math.min(length, pageSize - voffset);
		System.arraycopy(data, offset, memory, paddr, amount);
		if (amount < length)
			return amount + writeVirtualMemory(Processor.makeAddress(t.vpn + 1, 0), data, offset + amount, length - amount);
		return amount;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}
		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			if (!allocatePages(numPages, section.getLength(), section.isReadOnly())) {
				releasePages();
				return false;
			}
//			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			releasePages();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		if (!allocatePages(numPages, stackPages, false)) {
			releasePages();
			return false;
		}
//		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		if (!allocatePages(numPages, 1, false)) {
			releasePages();
			return false;
		}
//		numPages++;

		if (!loadSections()) {
			releasePages();
			return false;
		}

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		if (numPages > Machine.processor().getNumPhysPages()) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;

				TranslationEntry te = lookupPageTable(vpn);
				if (te == null)
					return false;
				section.loadPage(i, te.ppn);
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}
	
	private int openFile(int filename, boolean create) {
		String pathname = readVirtualMemoryString(filename, argLenMax);
		if (pathname == null) {
			return -1;
		}
		if (deleteFiles.contains(pathname))
			return -1;
		OpenFile f = ThreadedKernel.fileSystem.open(pathname, create);
		if (f == null) {
			return -1;
		}
		int fd = fdNum++;
		openFiles.put(f, pathname);
		fdinfo.put(new Integer(fd), f);
		return fd;
	}

	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {
		if (pid == 0) {
			Machine.halt();
			Lib.assertNotReached("Machine.halt() did not halt machine!");
		}
		return 0;
	}
	
	protected int handleExit(int status) {
		this.status = status;
		
		for (OpenFile f : fdinfo.values())
			f.close();
		coff.close();
		for (UserProcess p : childs.values()) {
			p.parent = null;
		}
		childs.clear();
		releasePages();
		
		lockProcess.acquire();
		proc.remove(new Integer(pid));
		boolean halt = proc.isEmpty();
		lockProcess.release();
		
		finished.V();
		
		if (halt)
			Kernel.kernel.terminate();
		else
			UThread.finish();
		return 0;
	}

	private int handleExec(int a0, int a1, int a2) {
		String filename = readVirtualMemoryString(a0, argLenMax);
		if (filename == null)
			return -1;
		if (a1 < 0)
			return -1;
		String[] args = new String[a1];
		for (int i = 0; i < a1; i++) {
			byte[] buffer = new byte[4];
			if (readVirtualMemory(a2 + i * 4, buffer) != 4) {
				return -1;
			}
			int addr = Lib.bytesToInt(buffer, 0);
			args[i] = readVirtualMemoryString(addr, argLenMax);
			if (args[i] == null)
				return -1;
		}
		UserProcess child = UserProcess.newUserProcess();
		if (!child.execute(filename, args)) {
			lockProcess.acquire();
			proc.remove(new Integer(child.pid));
			lockProcess.release();
			return -1;
		}
		childs.put(new Integer(child.pid), child);
		child.parent = this;
		return child.pid;
	}
	
	private int handleJoin(int a0, int a1) {
		lockProcess.acquire();
		UserProcess child = childs.get(new Integer(a0));
		lockProcess.release();
		if (child == null || child.parent != this) {
			return -1;
		}
		child.parent = null;
		childs.remove(new Integer(a0));
		child.finished.P();

		writeVirtualMemory(a1, Lib.bytesFromInt(child.status));
		if (child.status != -1) {
			return 1;
		}
		return 0;
	}
	
	private int handleCreat(int a0) {
		return openFile(a0, true);
	}
	
	private int handleOpen(int a0) {
		return openFile(a0, false);
	}
	
	private int handleRead(int a0, int a1, int a2) {
		OpenFile f = fdinfo.get(a0);
		if (f == null) {
			return -1;
		}
		if (a2 < 0) {
			return -1;
		}
		byte[] buffer = new byte[a2];
		int count = f.read(buffer, 0, a2);
		if (count == -1 || writeVirtualMemory(a1, buffer, 0, count) != count)
			return -1;
		return count;
	}
	
	private int handleWrite(int a0, int a1, int a2) {
		OpenFile f = fdinfo.get(a0);
		if (f == null) {
			return -1;
		}
		if (a2 < 0) {
			return -1;
		}
		byte[] buffer = new byte[a2];
		if (readVirtualMemory(a1, buffer, 0, a2) != a2)
			return -1;
		int count = f.write(buffer, 0, a2);
		return count;
	}
	
	private int handleClose(int a0) {
		if (!fdinfo.containsKey(new Integer(a0)))
			return -1;
		OpenFile f = fdinfo.remove(new Integer(a0));
		String pathname = openFiles.remove(f);
		if (deleteFiles.contains(pathname) && (!openFiles.containsValue(pathname))) {
			deleteFiles.remove(pathname);
			ThreadedKernel.fileSystem.remove(pathname);
		}
		f.close();
		return 0;
	}
	
	private int handleUnlink(int a0) {
		String pathname = readVirtualMemoryString(a0, argLenMax);
		if (pathname == null) {
			return -1;
		}
		if (openFiles.containsValue(pathname)) {
			deleteFiles.add(pathname);
			return -1;
		}
		if (!ThreadedKernel.fileSystem.remove(pathname))
			return -1;
		return 0;
	}


	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			return handleExec(a0, a1, a2);
		case syscallJoin:
			return handleJoin(a0, a1);
		case syscallCreate:
			return handleCreat(a0);
		case syscallOpen:
			return handleOpen(a0);
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		case syscallUnlink:
			return handleUnlink(a0);

		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			handleExit(-1);
			Lib.assertNotReached("Unknown system call!");
		}
		return 0;
	}


	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			handleExit(-1);
			Lib.assertNotReached("Unexpected exception");
		}
	}

	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	private int initialPC, initialSP;
	private int argc, argv;
	protected int pid;
	private int fdNum;
	private int status;
	private UserProcess parent;
	private Semaphore finished;
	
	private Map<Integer, OpenFile> fdinfo;
	private Map<Integer, UserProcess> childs;
	
	private static int pidNum = 0;
	private static Map<Integer, UserProcess> proc = new HashMap<Integer, UserProcess>();
	private static Map<OpenFile, String> openFiles = new HashMap<OpenFile, String>();
	private static Set<String> deleteFiles = new HashSet<String>();
	
	private static Lock lockProcess = new Lock();

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	
	protected static final int argLenMax = 256;
}
