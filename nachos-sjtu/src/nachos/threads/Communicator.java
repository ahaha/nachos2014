package nachos.threads;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		tot = 0;
		lock = new Lock();
		cspeaker = new Condition(lock);
		clistener = new Condition(lock);
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		lock.acquire();
		int id = tot++;
		speakers.put(id, word);
		if (listener > 0) {
			listener--;
			clistener.wake();
		}
		while (speakers.containsKey(id)) cspeaker.sleep();
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		lock.acquire();
		while (speakers.isEmpty()) {
			listener++;
			clistener.sleep();
		}
		int word = 0;
		for (Entry<Integer, Integer> e: speakers.entrySet()) {
			word = e.getValue();
			speakers.remove(e.getKey());
			break;
		}
		cspeaker.wakeAll();
		lock.release();
		return word;
	}
	
	private int tot, listener;
	private Map<Integer, Integer> speakers = new HashMap<Integer, Integer>();
	private Lock lock;
	private Condition clistener, cspeaker; 
}
