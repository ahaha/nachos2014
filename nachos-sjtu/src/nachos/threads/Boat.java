package nachos.threads;

import java.util.ArrayList;
import java.util.List;

import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;
	
	static private Lock lock;
	static private Condition cv;
	
	static int leftAdults, leftChildren;
	static boolean leftBoat, rightBoat;
	static boolean finish;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here
		lock = new Lock();
		cv = new Condition(lock);
		leftAdults = adults;
		leftChildren = children;
		leftBoat = true;
		rightBoat = false;
		finish = false;

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.
		
		List<KThread> threads = new ArrayList<KThread>();
		
		for (int i = 0; i < adults; i++) {
			KThread t = new KThread(new Runnable() {
				public void run() {
					AdultItinerary();
				}
			});
			t.setName("Adult #" + i);
			threads.add(t);
			t.fork();
		}
		
		for (int i = 0; i < children; i++) {
			KThread t = new KThread(new Runnable() {
				public void run() {
					ChildItinerary();
				}
			});
			t.setName("Child #" + i);
			threads.add(t);
			t.fork();
		}

		for (KThread t : threads)
			t.join();
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		lock.acquire();
		while (!leftBoat || leftChildren >= 2) {
			cv.sleep();
		}
		leftAdults--;
		if (leftAdults == 0 && leftChildren == 0)
			finish = true;
		bg.AdultRowToMolokai();
		leftBoat = false; rightBoat = true;
		cv.wakeAll();
		lock.release();
	}

	static void ChildItinerary() {
		boolean left = true;
		lock.acquire();
		while (!finish) {
			if (left) {
				if (leftBoat) {
					if (leftChildren >= 2 || (leftAdults == 0 && leftChildren == 1)) {
						left = false;
						leftBoat = false;
						leftChildren--;
						if (leftAdults == 0 && leftChildren == 0)
							finish = true;
						bg.ChildRideToMolokai();
						cv.wakeAll();
					} else cv.sleep();
				} else if (!rightBoat) {
					left = false;
					rightBoat = true;
					leftChildren--;
					if (leftAdults == 0 && leftChildren == 0)
						finish = true;
					bg.ChildRideToMolokai();
					cv.wakeAll();
				} else cv.sleep();
			} else {
				if (rightBoat) {
					left = true;
					leftBoat = true;
					rightBoat = false;
					leftChildren++;
					bg.ChildRideToOahu();
					cv.wakeAll();
				} else cv.sleep();
			}
		}
		lock.release();
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

}
