package nachos.threads;

import java.util.HashSet;
import java.util.Set;

import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
		super();
	}

	/**
	 * Allocate a new lottery thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer tickets from
	 *            waiting threads to the owning thread.
	 * @return a new lottery thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryQueue(transferPriority);
	}
	
	protected ThreadState getThreadState(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new ThreadState(thread);

		return (ThreadState) thread.schedulingState;
	}
	
	private class LotteryQueue extends PriorityQueue {

		LotteryQueue(boolean transferPriority) {
			super(transferPriority);
		}
		
		@Override
		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			ThreadState state = pickNextThread();
			if (owner != null && getThreadState(owner.thread) != null)
				getThreadState(owner.thread).waitingList.remove(this);
			if (state != null) {
				waitQueue.remove(state);
				state.acquire(this);
				return state.thread;
			}
			return null;
		}
		
		@Override
		public ThreadState pickNextThread() {
			KThread nextThread = null;
			int sum = 0;
			for (KThread t : waitQueue) {
				sum += getEffectivePriority(t);
			}
			if (sum != 0) {
				int choice = Lib.random(sum);
				sum = 0;
				for (KThread t : waitQueue) {
					sum += getEffectivePriority(t);
					if (choice < sum)
						nextThread = t;
				}
			}
			if (nextThread != null)
				return getThreadState(nextThread);
			return null;
		}
	}
	
	private class ThreadState extends PriorityScheduler.ThreadState {

		public ThreadState(KThread thread) {
			super(thread);
		}
		
		public int getEffectivePriority() {
			return getEffectivePriority(new HashSet<ThreadState>());
		}

		private int getEffectivePriority(Set<ThreadState> visited) {
			if (visited.contains(this)) {
				return priority;
			}
			visited.add(this);
			effectivePriority = priority;
			for (PriorityQueue queue : waitingList) {
				if (queue.transferPriority)
					for (KThread thread : queue.waitQueue) {
						int p = getThreadState(thread).getEffectivePriority(visited);
							effectivePriority += p;
					}
			}
			visited.remove(this);
			return effectivePriority;
		}

	}
}
